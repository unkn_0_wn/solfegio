const {Router} = require('express');
const router = Router();



router.get('/', (req,res)=>{
    res.render('biography.ejs', {
        title: '𝄞 Біографія',
        name: 'Biography',
        is: 'Biography'
    });
})


module.exports  = router;
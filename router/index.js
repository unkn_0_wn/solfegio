const {Router} = require('express');
const router = Router();



router.get('/', (req,res)=>{
    res.render('index.ejs', {
        title: '𝄞 EL SOLFEGIO',
        name: 'home',
        is: 'home'
    });
})


module.exports  = router;
const {Router} = require('express');
const router = Router();



router.get('/', (req,res)=>{
    res.render('price.ejs', {
        title: '𝄞 Price',
        name: 'Price',
        is: 'Price'
    });
})


module.exports  = router;
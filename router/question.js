const {Router} = require('express');
const router = Router();

router.get('/', (req,res)=>{
    res.render('question.ejs', {
        title: '𝄞 Питання',
        name: 'question',
        is: 'question'
    });
})


module.exports  = router;
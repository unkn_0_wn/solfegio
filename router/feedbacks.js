const {Router} = require('express');
const router = Router();

router.get('/', (req,res)=>{
    res.render('feedbacks.ejs', {
        title: '𝄞 Відгуки',
        name: 'Biography',
        is: 'Biography'
    });
})


module.exports  = router;
// const browser = require('browser-sync').create('Server');;
const express = require('express');
const ip = require('ip').address();
const routerIndex = require('./router/index');
const routerBiography = require('./router/biography');
const routerPrice = require('./router/price');
const routerFeedbacks = require('./router/feedbacks');
const routerContact = require('./router/contact');
const routerQuestion = require('./router/question');



console.log(ip)
const PORT = process.env.PORT || 3000;
console.log(PORT)
const app = express();

app.set('views', __dirname + '/view');
app.set("view options", { layout: "mylayout.jade" });
app.set('view engine', 'ejs');

app.use(express.static("public"));

app.use('/', routerIndex);
app.use('/biography', routerBiography);
app.use('/price', routerPrice);
app.use('/feedbacks', routerFeedbacks);
app.use('/contact', routerContact);
app.use('/question', routerQuestion);

async function start(){
    try{
        app.listen(PORT, ip, ()=>{
            console.log(`Server is up ${PORT}`)
        })
        // browser.init({
        //     server: "index.js"
        // });
        // browser.reload();
    }catch(e){
        console.log(e);
    }

}
start();
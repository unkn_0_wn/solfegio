let position = 0;
const SlideToShow = 1;
const SlideToScroll = 1;
const container = document.querySelector('.bk_slide');
const track =  document.querySelector('.slider_trek');
const items =  document.querySelectorAll('.slide_1');
const itemsCount =  items.length;
const btnPrev =  document.querySelector('.pre__slaide');
const btnNext =  document.querySelector('.next__slide');
const itemWidth = container.clientWidth / SlideToShow;
const movePosition = SlideToScroll * itemWidth;
items.forEach(item => {
   item.style.minWidth = `${itemWidth}px`
})

btnNext.addEventListener('click', (e)=>{
    e.preventDefault();

    const itemsLeft = itemsCount - (Math.abs(position) + SlideToShow * itemWidth) / itemWidth;

    if(position <= -(itemsCount - SlideToShow) * itemWidth){
        position = 0;
    }

    position -= itemsLeft >= SlideToScroll ? movePosition : itemsLeft * itemWidth;

    setPosition();
    clearInterval(timerId);
    timerId = setTimeout(nSlide, 2500);
})

btnPrev.addEventListener('click', (e)=>{
    e.preventDefault();

    const itemsLeft =Math.abs(position) / itemWidth;

    position += itemsLeft >= SlideToScroll ? movePosition : itemsLeft * itemWidth;

    setPosition();
    BtnsCheck();
    clearInterval(timerId);
    timerId = setTimeout(nSlide, 2500);
})

const setPosition = () =>{
    track.style.transform = `translateX(${position}px)`;
}

const BtnsCheck = () => {
    btnPrev.disable = position === 0;
}

function nSlide() {
    const itemsLeft = itemsCount - (Math.abs(position) + SlideToShow * itemWidth) / itemWidth;
    if(position <= -(itemsCount - SlideToShow) * itemWidth){
        position = 0;
      }
    position -= itemsLeft >= SlideToScroll ? movePosition : itemsLeft * itemWidth;
    setPosition();
    timerId = setTimeout(nSlide, 2500); // (*)
  }
  
let timerId = setTimeout(nSlide, 2500);


BtnsCheck();
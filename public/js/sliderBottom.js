function f(){
let position_b = 0;
const SlideToShow = 1;
const SlideToScroll = 1;
const container = document.querySelector('.slider_container_b');
const track =  document.querySelector('.slider_track_b');
const items =  document.querySelectorAll('.slides');
const itemsCount =  items.length;
const btnPrev =  document.querySelector('.pre_slide_feed');
const btnNext =  document.querySelector('.next_slide_feed');
const itemWidth = container.clientWidth / SlideToShow;
const movePosition = SlideToScroll * itemWidth;
items.forEach(item => {
   item.style.minWidth = `${itemWidth}px`
})

btnNext.addEventListener('click', (e)=>{
    e.preventDefault();

    const itemsLeft = itemsCount - (Math.abs(position_b) + SlideToShow * itemWidth) / itemWidth;

    if(position_b <= -(itemsCount - SlideToShow) * itemWidth){
        position_b = 0;
    }

    position_b -= itemsLeft >= SlideToScroll ? movePosition : itemsLeft * itemWidth;

    setPosition();
    clearInterval(timerId);
    timerId = setTimeout(nSlide, 2500);
})

btnPrev.addEventListener('click', (e)=>{
    e.preventDefault();

    const itemsLeft =Math.abs(position_b) / itemWidth;

    position_b += itemsLeft >= SlideToScroll ? movePosition : itemsLeft * itemWidth;

    setPosition();
    BtnsCheck();
    clearInterval(timerId);
    timerId = setTimeout(nSlide, 2500);
})

const setPosition = () =>{
    track.style.transform = `translateX(${position_b}px)`;
}

const BtnsCheck = () => {
    btnPrev.disable = position_b === 0;
}

function nSlide() {
    const itemsLeft = itemsCount - (Math.abs(position_b) + SlideToShow * itemWidth) / itemWidth;
    if(position_b <= -(itemsCount - SlideToShow) * itemWidth){
        position_b = 0;
      }
      position_b -= itemsLeft >= SlideToScroll ? movePosition : itemsLeft * itemWidth;
    setPosition();
    timerId = setTimeout(nSlide, 2500); // (*)
  }
  
let timerId = setTimeout(nSlide, 2500);


BtnsCheck();

};f()